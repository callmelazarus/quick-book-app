from django.contrib import admin
from bookapp.models import Book 
# Register your models here.

admin.site.register(Book)

# class BookAdmin(Book): # this is creating a subclass of Book
#     pass

# admin.site.register(BookAdmin)   # this is how you actually register this