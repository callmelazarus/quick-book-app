from django.shortcuts import render
from bookapp.models import Book #imported the new model we created
# Create your views here.

def show_books(request):
    list_books = Book.objects.all() # book will get hte query set of books
    context = {     #context needs to be a dictionary
        "books": list_books # assigns the list like structure in list_books to the key 'books'
    }
    return render(request, "books/list.html", context) #path is information after the templates

    #any key that you pass on in the render function, is available in the template as a variable