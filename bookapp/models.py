from pyexpat import model
from django.db import models #need to add this line to import

# Create your models here.
class Book(models.Model):
    title = models.CharField(max_length=100) # attributes of the class. this INSTANTIATES one instance of the Book class
    author = models.CharField(max_length=50) 
    num_pages = models.IntegerField()
    ISBN = models.IntegerField()
    cover = models.URLField(null=True, blank = True) #null keyword argument = can the value of this attribute be null/no value in the field within the databanse? T/F. 
    in_print = models.BooleanField()                 #blank keyword argument = is the form on the admin page, can that field be left blank, and the field be a valid submission of the form? T/F
    publish_date = models.IntegerField()

    #this function below, is used by the admin page, to describe the instance in the admin interface
    def __str__(self): #represent an instance of this model as a string - > it will return what you have noted
        return self.title + " by " + self.author