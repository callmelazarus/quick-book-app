from django.urls import path
from bookapp.views import show_books #need to import the view function

urlpatterns = [
    path("", show_books, name = "book list")  #1st arg: path. 2nd arg: view function
]

# path notes:
# "" signifies no additional path
